﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource m_pSource;
    private bool m_bLooping;

    public void init( AudioClip pClip, bool bLoop )
    {
        m_bLooping = bLoop;

        m_pSource = gameObject.AddComponent<AudioSource>();
        m_pSource.clip = pClip;
        m_pSource.Play();
        m_pSource.loop = bLoop;
    }

    public void destructor()
    {
        Destroy(this);
        Destroy(m_pSource);
    }

    public void Update()
    {
        if (m_bLooping || m_pSource.isPlaying)
            return;

        destructor();
    }

    public static SoundManager Create( GameObject pObj, string szClip, bool bLoop )
    {
        SoundManager pSound = pObj.AddComponent<SoundManager>();
        AudioClip pClip = Resources.Load<AudioClip>("Sound FX/" + szClip);
        pSound.init(pClip, bLoop);

        return pSound;
    }
}