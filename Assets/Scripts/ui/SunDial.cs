﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunDial : MonoBehaviour {

    private const float m_nXOffset = -25;
    private const float m_nYOffset = -36;
    private const float BORDERWIDTH = 35;
    private RectTransform m_pPointer;
    private float m_nBarWidth;

	void Awake () {
        m_pPointer = transform.Find("SunPointer").GetComponent<RectTransform>();
        m_nBarWidth = GetComponent<RectTransform>().sizeDelta.x - BORDERWIDTH;
    }
	
	void Update ()
    {
        float nPerc = Game.scenery.m_nHoursSinceStart / 12;
        float nPos = m_nBarWidth * nPerc + m_nXOffset;

        m_pPointer.anchoredPosition = new Vector2(nPos, m_nYOffset);
    }
}
