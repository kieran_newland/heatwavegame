﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayBtn : MonoBehaviour {
    
	void Awake () {
        Button pBtn = GetComponent<Button>();
        pBtn.onClick.AddListener(() => onBtnDown());
	}
	
	private void onBtnDown()
    {
        SceneManager.LoadScene("level");
    }
}
