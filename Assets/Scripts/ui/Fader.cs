﻿using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    public delegate void onFaded();
    private onFaded m_cbOnFaded;

    private bool m_bFading;
    private Image m_pImage;

    private const float FADESPEED = 1;
    private float m_nCurrentFade;

    public void Awake()
    {
        Game.fader = this;
        m_pImage = GetComponent<Image>();
    }

    public void fade( bool bFade, onFaded cbOnFaded )
    {
        m_bFading = bFade;
        m_cbOnFaded = cbOnFaded;

        if (m_bFading)
            setColour(1);
        else
            setColour(0);

        setToFront();
    }

    public void Update()
    {
        float nDirection = m_bFading ? -1 : 1;
        float nFadeDiff = Time.deltaTime * FADESPEED * nDirection;
        float nNewFade = Mathf.Clamp01( m_nCurrentFade + nFadeDiff );

        if (m_cbOnFaded != null)
        {
            if (nNewFade == 0 && m_bFading)
                m_cbOnFaded();
            if (nNewFade == 1 && !m_bFading)
                m_cbOnFaded();
        }


        setColour(nNewFade);
    }

    private void setColour( float nAlpha )
    {
        m_nCurrentFade = nAlpha;
        m_pImage.color = new Color(0, 0, 0, nAlpha);
    }

    public void setToFront()
    {
        transform.SetAsLastSibling();
    }
}