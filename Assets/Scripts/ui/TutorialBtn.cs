﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialBtn : MonoBehaviour {

    private SpriteRenderer m_pBgRenderer;
    private Sprite m_pStartScreen;
    private Sprite m_pTutorialScreen;
    private GameObject m_pCanvas;
    private GameObject m_pTitle;

    private bool m_bEnabled;
    private bool m_bJustClicked;

	void Awake () {
        Button pBtn = GetComponent<Button>();
        pBtn.onClick.AddListener(() => onBtnDown());

        m_pBgRenderer = GameObject.FindGameObjectWithTag("TitleBg").GetComponent<SpriteRenderer>();
        m_pStartScreen = m_pBgRenderer.sprite;

        m_pTutorialScreen = Resources.Load<Sprite>("TutorialScreen");

        m_pCanvas = GameObject.FindGameObjectWithTag("Canvas");
        m_pTitle = GameObject.FindGameObjectWithTag("Title");
    }
	
	private void onBtnDown()
    {
        setEnabled(!m_bEnabled);
        m_bJustClicked = true;
    }

    private void setEnabled( bool bEnabled )
    {
        m_bEnabled = bEnabled;

        Image[] aRenderers = m_pCanvas.GetComponentsInChildren<Image>();
        for (int i = 0; i < aRenderers.Length; i++)
            aRenderers[i].enabled = !bEnabled;

        m_pTitle.SetActive(!bEnabled);

        if ( bEnabled )
            m_pBgRenderer.sprite = m_pTutorialScreen;
        else
            m_pBgRenderer.sprite = m_pStartScreen;
    }

    public void Update()
    {
        if (m_bEnabled && Input.GetMouseButtonUp(0))
        {
            if(m_bJustClicked)
            {
                m_bJustClicked = false;
                return;
            }

            setEnabled(false);
        }
    }
}
