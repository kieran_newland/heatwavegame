﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FakeBloom : MonoBehaviour {

    private CanvasRenderer m_pImage;

	void Start () {
        m_pImage = GetComponent<CanvasRenderer>();
    }
	
	void Update () {

        float nDayProgress = Game.scenery.m_nHoursSinceStart / 12;

        Color pStartCol = new Color(0.25f, 0.25f, 0.18f, 0.4f);
        Color pMiddayCol = new Color( 1, 0.8f, 0, 0.55f );
        Color pNightCol = new Color(0, 0, 0.05f, 0.55f);

        Color pFinalCol;

        //Blend until peak of day, 3pm
        if (nDayProgress < 2.0f / 3)
        {
            Debug.Log(nDayProgress * 1.5f);
            pFinalCol = Color.Lerp(pStartCol, pMiddayCol, nDayProgress * 1.5f);
        }
        else
            pFinalCol = Color.Lerp(pMiddayCol, pNightCol, (nDayProgress - 0.66f) * 3);

        Debug.Log(pFinalCol);

        m_pImage.SetColor(pFinalCol);
	}
}
