﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndOfGameDlg : MonoBehaviour
{
    public void init( float nScore )
    {
        transform.Find("ScoreText").GetComponent<Text>().text = "Level " + Game.level + " - Score: " + nScore;
        transform.Find("RestartLevelBtn").GetComponentInChildren<Button>().onClick.AddListener(() => onRestart());
        transform.Find("NextLevelBtn").GetComponentInChildren<Button>().onClick.AddListener(() => onNextLevel());
    }

    private void onRestart()
    {
		SoundManager.Create (gameObject, "clickmp3", false);
        Game.fader.fade(false, refreshScene);
    }

    private void onNextLevel()
    {
        Game.level++;

        if (Game.level > 10)
            Game.level = 10;

		SoundManager.Create (gameObject, "clickmp3", false);
        refreshScene();
    }

    private void refreshScene()
    {
        Debug.Log("Changing scene");
        SceneManager.LoadScene("level");
    }

    public static EndOfGameDlg Create( float nScore )
    {
        GameObject pDialogObject = Instantiate(Resources.Load<GameObject>("dialogs/EndOfGameDlg"));
        GameObject pCanvas = GameObject.FindGameObjectWithTag("Canvas");

        pDialogObject.transform.SetParent(pCanvas.transform, false);

        EndOfGameDlg pDialog = pDialogObject.GetComponent<EndOfGameDlg>();
        pDialog.init(nScore);

        return pDialog;
    }
}