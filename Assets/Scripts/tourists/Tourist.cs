﻿using UnityEngine;
using System.Collections.Generic;
//using SpriterDotNetUnity;

public class Tourist : MonoBehaviour
{
    private static int m_nInputLayer;

    //Walking
    public const float WALKSPEED = 1;
    public float m_nStartX { get; private set; }

    public Animator m_pAnimator { get; private set; }
    public BoxCollider2D m_pCollider { get; private set; }

    //Input
    private bool m_bPickedUp;
    private bool m_bInputDisabled;
    private Vector3 m_pStartClickPos;
    private const float MOUSEMOVEAMT = 20;

    private DroppableArea m_pCurrentArea;

    //States
    private List<TouristState> m_aStates;
    private TouristState m_pCurrentState;

    //Tan
    private const float TANSPEED = 0.1f;
    private List<Material> m_aTanShaders;
    public float m_nFrontTan { get; private set; }
    public float m_nBackTan { get; private set; }
	private bool m_bSizzling;
	private bool m_bBurning;
    public bool m_bLayingOnFront { get; private set; }

    //Thirst
    private const float THIRSTSPEED = 0.04f;
    private const float COOLDOWNSPEED = 0.1f;
    public float m_nThirst { get; private set; }
    private ThirstBar m_pThirstBar;

    public void Awake()
    {
        if (m_nInputLayer == 0)
            m_nInputLayer = LayerMask.NameToLayer("Input");

        m_bLayingOnFront = true;
        m_pAnimator = GetComponent<Animator>();
        m_pCollider = GetComponent<BoxCollider2D>();
        new TouristRandomiser(this);

        setUpStates();
        setState(TouristState.StateId.ENTER);

        //Create thirst bar
        GameObject pCanvas = GameObject.FindGameObjectWithTag("Canvas");

        GameObject pThirstBarObj = Instantiate(Resources.Load<GameObject>("ThirstBar"));
        pThirstBarObj.transform.SetParent(pCanvas.transform, false);

        m_pThirstBar = pThirstBarObj.GetComponent<ThirstBar>();
        m_pThirstBar.setFollowTarget(transform);

        saveShader();
    }

    public void OnDestroy()
    {
        if( m_pThirstBar != null && m_pThirstBar.gameObject != null )
            Destroy(m_pThirstBar.gameObject);
    }

    public void switchTanSides()
    {
        m_bLayingOnFront = !m_bLayingOnFront;
        setTanCol();
    }

    private void saveShader()
    {
        const string SHADERNAME = "Custom/Tan";

        m_aTanShaders = new List<Material>();

        Material pShader;
        SpriteRenderer pRenderer;
        SpriteRenderer[] aRenderers = GetComponentsInChildren<SpriteRenderer>();

        for( int i = 0; i < aRenderers.Length; i++)
        {
            pRenderer = aRenderers[i];

            if (pRenderer.material.shader.name != SHADERNAME)
                continue;

            pShader = Instantiate(pRenderer.material);
            pRenderer.material = pShader;

            m_aTanShaders.Add(pShader);
        }
    }

    public void setStartPos( Vector3 pPos )
    {
        m_nStartX = pPos.x;

        Vector3 pLeftOfScreen = Camera.main.ScreenToWorldPoint(Vector3.zero);
        transform.position = new Vector3(pLeftOfScreen.x - m_pCollider.bounds.size.x, pPos.y);
    }

    private void setUpStates()
    {
        m_aStates = new List<TouristState>();

        m_aStates.Add(new TouristEnterState(this));
        m_aStates.Add(new TouristIdleState( this ));
        m_aStates.Add(new TouristSunBatheState(this));
        m_aStates.Add(new TouristSwimState(this));
        m_aStates.Add(new TouristLeaveState(this));
        m_aStates.Add(new TouristLeaveEarlyState(this));
        m_aStates.Add(new TouristDeadState(this));
        m_aStates.Add(new TouristHoldingState(this));
    }

    public void setState( TouristState.StateId pState )
    {
        if (m_pCurrentState != null)
            m_pCurrentState.onStateExit();

        m_pCurrentState = null;

        //Find new state
        for( int i = 0; i < m_aStates.Count; i++ )
        {
            if (m_aStates[i].m_pStateId != pState)
                continue;

            m_pCurrentState = m_aStates[i];
            break;
        }

        if (m_pCurrentState == null)
            Debug.LogError("Cannot find state "  + pState);

        m_pCurrentState.onStateEnter();
    }

    public void onEvent( TouristEvent pEvent )
    {
        if( m_pCurrentState != null )
            m_pCurrentState.onEvent(pEvent);
    }

    public void onMouseDown()
    {
        if (m_bInputDisabled)
        {
            Debug.Log("Input is disabled");
            return;
        }

        Debug.Log(m_pCurrentState.m_pStateId);

        m_pStartClickPos = Input.mousePosition;

        setState(TouristState.StateId.HOLDING);
        m_bPickedUp = true;
    }

    public void onMouseUp()
    {
        if (m_bInputDisabled)
            return;

        if (!m_bPickedUp)
            return;

        m_bPickedUp = false;

        if (m_pCurrentArea != null)
            m_pCurrentArea.onTouristLeave( this );

        placeOnNewArea();
    }

    private void placeOnNewArea()
    {
        DroppableArea pArea = getDroppableAreaUnderMouse();

        //If dropped on to the sand, revert to idle
        if (pArea == null)
        {
            setState(TouristState.StateId.IDLE);
			SoundManager.Create (gameObject, "dropmp3", false);
            return;
        }

        m_pCurrentArea = null;

        //Make sure the target is not already in use
        if (!pArea.setUpTourist(this))
        {
            setState(TouristState.StateId.IDLE);
			SoundManager.Create (gameObject, "dropmp3", false);
            return;
        }

        if (Vector3.Distance(Input.mousePosition, m_pStartClickPos) < MOUSEMOVEAMT)
            onEvent(TouristEvent.SAMEAREA);

        m_pCurrentArea = pArea;
    }

    private DroppableArea getDroppableAreaUnderMouse()
    {
        RaycastHit2D pHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward, Mathf.Infinity, (1 << m_nInputLayer));
        if (pHit.collider == null)
            return null;

        return pHit.collider.GetComponent<DroppableArea>();
    }

    public void disableInput()
    {
        m_bInputDisabled = true;
    }

    public void Update()
    {
        if( Input.GetMouseButtonDown( 0 ) )
        {
            RaycastHit2D pHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward, Mathf.Infinity, (1 << LayerMask.NameToLayer("Tourist")));

            if (pHit.collider != null && pHit.collider.gameObject == gameObject)
                onMouseDown();
        }

        if (Input.GetMouseButtonUp(0))
            onMouseUp();

        m_pCurrentState.onUpdate();

        if (m_bPickedUp)
            handleMouseMove();
    }

    private void handleMouseMove()
    {
        Vector3 pWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        transform.position = pWorldPos;

        setZValue();
    }

    public void setZValue()
    {
        float nZValue = (transform.position.y / 10) - 0.5f;
        Vector3 pNewPos = transform.position;
        pNewPos.z = nZValue;

        transform.position = pNewPos;
    }

    public void increaseTan()
    {
        float nAmt = TANSPEED * Time.deltaTime;

        if (m_bLayingOnFront)
            m_nFrontTan += nAmt;
        else
            m_nBackTan += nAmt;

		if ((m_nFrontTan > 2 || m_nBackTan > 2) && m_pCurrentState.m_pStateId != TouristState.StateId.DEAD) 
			setState (TouristState.StateId.DEAD);
			
		if (m_nFrontTan > 1.3 || m_nBackTan > 1.3) 
		{
			if (m_nFrontTan > 1.6 || m_nBackTan > 1.6) 
			{
				if (m_bSizzling)
					return;

				m_bSizzling = true;
				SoundManager.Create (gameObject, "sizzlemp3", false);
				return;
			}

			if (m_bBurning)
				return;
			SoundManager.Create (gameObject, "flamemp3", false);
			m_bBurning = true;
		}

        setTanCol();

    }

    public void increaseThirst()
    {
        m_nThirst = Mathf.Clamp01( m_nThirst + THIRSTSPEED * Time.deltaTime );

        m_pThirstBar.setThirstAmt(m_nThirst);

        m_pAnimator.SetFloat("nThirst", m_nThirst);

        if (m_nThirst >= 1 && m_pCurrentState.m_pStateId != TouristState.StateId.DEAD)
            setState(TouristState.StateId.DEAD);
    }

    public void decreaseThirst()
    {
        m_nThirst = Mathf.Clamp01(m_nThirst - COOLDOWNSPEED * Time.deltaTime);

        m_pThirstBar.setThirstAmt(m_nThirst);
    }

    public void setTanCol()
    {
        float nTan = 0;

        if (m_bLayingOnFront)
            nTan = m_nFrontTan;
        else
            nTan = m_nBackTan;

        nTan = Mathf.Clamp(nTan, 0, 2);

        for( int i = 0; i < m_aTanShaders.Count; i++)
            m_aTanShaders[i].SetFloat("_TanAmt", nTan);

        m_pAnimator.SetFloat("nTan", nTan);
    }

    public static Tourist Create( Vector3 pPos )
    {
        //TODO Create tourist from loaded object
        GameObject pTouristObj = Instantiate( Resources.Load<GameObject>("TouristPrefab") );

        Tourist pTourist = pTouristObj.AddComponent<Tourist>();
        pTourist.setStartPos(pPos);

        return pTourist;
    }
}