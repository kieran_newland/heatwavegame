﻿using UnityEngine;
using System.Collections.Generic;

public class TouristManager : MonoBehaviour
{
    private List<Tourist> m_aTourists;
    private SpawnPoint m_pSpawnPoint;
    private List<float> m_aArrivals;

    public void Awake()
    {
        m_pSpawnPoint = GameObject.FindGameObjectWithTag("Spawn").GetComponent<SpawnPoint>();

        m_aTourists = new List<Tourist>();
    }

    public void init(float[] aArrivals)
    {
        m_aArrivals = new List<float>();
        m_aArrivals.AddRange(aArrivals);
    }

    public void Update()
    {
        if (m_aArrivals.Count == 0)
            return;

        float nNextArrival = m_aArrivals[0];

        if (Game.scenery.m_nHoursSinceStart < nNextArrival)
            return;

        createTourist();
        m_aArrivals.RemoveAt(0);
    }

    private void createTourist()
    {
        Tourist pTourist = Tourist.Create( m_pSpawnPoint.getRandomPoint() );
        pTourist.transform.parent = transform;
        pTourist.setZValue();
        m_aTourists.Add(pTourist);
    }

    public static TouristManager Create(float[] aArrivals)
    {
        GameObject pManagerObject = new GameObject("TouristManager");

        TouristManager pManager = pManagerObject.AddComponent<TouristManager>();
        pManager.init(aArrivals);

        return pManager;
    }
}