﻿using UnityEngine;
using System.Collections.Generic;

public class TouristRandomiser
{
    public TouristRandomiser( Tourist pTourist )
    {
        randomiseLayer(pTourist.transform, LayerMask.NameToLayer("Hair"));
        randomiseLayer(pTourist.transform, LayerMask.NameToLayer("Shorts"));
        randomiseLayer(pTourist.transform, LayerMask.NameToLayer("Head"));
        randomiseLayer(pTourist.transform, LayerMask.NameToLayer("Body"));
    }

    private void randomiseLayer( Transform pParent, int nLayer )
    {
        List<SpriteRenderer> aSameType = new List<SpriteRenderer>();
        SpriteRenderer[] aChildren = pParent.GetComponentsInChildren<SpriteRenderer>();

        //Find all children that count
        SpriteRenderer pChild;
        for( int i = 0; i < aChildren.Length; i++ )
        {
            pChild = aChildren[i];

            if (pChild.gameObject.layer != nLayer)
                continue;

            aSameType.Add(pChild);
        }

        //Only activate one of them
        int nRandom = Mathf.FloorToInt(Random.value * aSameType.Count);
        for( int i = 0; i < aSameType.Count; i++)
            aSameType[i].gameObject.SetActive(i == nRandom);
    }
}