﻿using UnityEngine;
using UnityEngine.UI;

public class ThirstBar : MonoBehaviour
{
    private RectTransform m_pParentTransform;
    private RectTransform m_pBarImg;
    private GameObject m_pWarning;
    private float m_nBarWidth;
    private Transform m_pFollowTarget;
    private Vector3 m_pOffset;

    private const float m_nYOffset = 80;

    public void Awake()
    {
        m_pWarning = transform.Find("Warning").gameObject;
        m_pParentTransform = GetComponent<RectTransform>();
        m_pBarImg = transform.Find("Bar").GetComponent<RectTransform>();
        m_nBarWidth = m_pBarImg.sizeDelta.x;

        setThirstAmt(0);
    }

    public void setFollowTarget( Transform pFollow )
    {
        m_pFollowTarget = pFollow;
    }

    public void setThirstAmt( float nAmt )
    {
        m_pBarImg.sizeDelta = new Vector2(nAmt * m_nBarWidth, m_pBarImg.sizeDelta.y);

        m_pWarning.SetActive(nAmt > 0.6f);
    }

    public void Update()
    {
        m_pParentTransform.anchoredPosition = Camera.main.WorldToScreenPoint(m_pFollowTarget.position) + new Vector3( -m_nBarWidth / 2, m_nYOffset );
    }
}