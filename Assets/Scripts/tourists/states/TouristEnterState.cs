﻿using UnityEngine;

public class TouristEnterState : TouristState
{
    public TouristEnterState( Tourist pTourist ) : base( StateId.ENTER, pTourist)
    {

    }

    public override void onUpdate()
    {
        Vector3 pNewPos = m_pTourist.transform.position + new Vector3(Tourist.WALKSPEED * Time.deltaTime, 0);
        m_pTourist.transform.position = pNewPos;

        if (pNewPos.x > m_pTourist.m_nStartX)
            m_pTourist.setState(StateId.IDLE);
    }
}