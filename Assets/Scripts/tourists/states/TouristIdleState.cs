﻿using UnityEngine;

public class TouristIdleState : TouristState
{
    private const float TIMETOWAIT = 15;
    private float m_nTimeWaiting;

    public TouristIdleState( Tourist pTourist ) : base( StateId.IDLE, pTourist)
    {

    }

    public override void onStateEnter()
    {
        m_nTimeWaiting = 0;
    }

    public override void onUpdate()
    {
        m_nTimeWaiting += Time.deltaTime;

        m_pTourist.increaseThirst();

        if (m_nTimeWaiting > TIMETOWAIT)
            m_pTourist.setState(StateId.LEAVEEARLY);
    }
}