﻿using UnityEngine;

public class TouristLeaveState : TouristState
{
    public TouristLeaveState( Tourist pTourist ) : base( StateId.LEAVE, pTourist )
    {

    }

    public override void onStateEnter()
    {
        m_pTourist.disableInput();

        Game.score.updateScore(m_pTourist.m_nFrontTan, m_pTourist.m_nBackTan);
    }

    public override void onUpdate()
    {
        m_pTourist.transform.position += new Vector3(Tourist.WALKSPEED * Time.deltaTime, 0);

        //Destroy the player when off screen
        int nScreenWidth = Screen.width;
        Vector3 pWorldEdge = Camera.main.ScreenToWorldPoint(new Vector3(nScreenWidth + 40, 0));

        
        if (m_pTourist.transform.position.x > pWorldEdge.x)
            Object.Destroy(m_pTourist.gameObject);
    }
}