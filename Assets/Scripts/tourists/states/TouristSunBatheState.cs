﻿using UnityEngine;

public class TouristSunBatheState : TouristState
{
    public TouristSunBatheState( Tourist pTourist ) : base(StateId.SUNBATHE, pTourist)
    {
        setSunBatheAnim();
    }

    public override void onStateEnter()
    {
        m_pAnimator.SetBool("bSunBathe", true);
    }

    public override void onStateExit()
    {
        m_pAnimator.SetBool("bSunBathe", false);
    }

    public override void onEvent(TouristEvent pEvent)
    {
        if (pEvent == TouristEvent.SAMEAREA)
            switchSides();
    }

    private void switchSides()
    {
        m_pTourist.switchTanSides();
        setSunBatheAnim();
    }

    private void setSunBatheAnim()
    {
        m_pAnimator.SetBool("bSunBatheFront", m_pTourist.m_bLayingOnFront );
    }

    public override void onUpdate()
    {
        m_pTourist.increaseTan();
        m_pTourist.increaseThirst();
    }
}