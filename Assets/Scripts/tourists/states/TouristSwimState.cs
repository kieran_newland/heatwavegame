﻿using UnityEngine;

public class TouristSwimState : TouristState
{
    private float m_nDirection;
    private const float SWIMSPEED = 0.4f;

    public TouristSwimState( Tourist pTourist ) : base( StateId.SWIM, pTourist )
    {

    }

    public override void onStateEnter()
    {
        changeDirection();
        m_pAnimator.SetBool("bSwimming", true);
    }

    public override void onStateExit()
    {
        m_pAnimator.SetBool("bSwimming", false);
    }

    private void changeDirection()
    {
        m_nDirection = Random.value * Mathf.PI * 2;
    }

    public override void onUpdate()
    {
        m_pTourist.decreaseThirst();

        //Swim in a random direction
        Vector3 pDirection = new Vector3(Mathf.Cos(m_nDirection), Mathf.Sin(m_nDirection));
        Vector3 pNewPos = m_pTourist.transform.position + (pDirection * Time.deltaTime * SWIMSPEED);

        if (!Game.scenery.m_pWater.isInWater( pNewPos ))
        {
            changeDirection();
            return;
        }

        m_pTourist.transform.position = pNewPos;
    }
}