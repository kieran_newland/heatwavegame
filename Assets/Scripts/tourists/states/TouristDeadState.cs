﻿using UnityEngine;

public class TouristDeadState : TouristState
{
    public TouristDeadState( Tourist pTourist ) : base( StateId.DEAD, pTourist )
    {

    }

    public override void onStateEnter()
    {
        m_pTourist.disableInput();
        m_pAnimator.SetBool("bDead", true);

        SoundManager.Create(m_pTourist.gameObject, "deadmp3", false);
    }

    public override void onUpdate()
    {
        m_pTourist.increaseTan();
    }
}