﻿using UnityEngine;

public class TouristLeaveEarlyState : TouristState
{
    private int LOOPAMT = 2;

    public TouristLeaveEarlyState( Tourist pTourist ) : base( StateId.LEAVEEARLY, pTourist )
    {

    }

    public override void onStateEnter()
    {
        m_pTourist.disableInput();

        for( int i = 0; i < LOOPAMT; i++)
            Game.score.updateScore(m_pTourist.m_nFrontTan, m_pTourist.m_nBackTan);
    }

    public override void onUpdate()
    {
        Vector3 pDirection = new Vector3(-Tourist.WALKSPEED * Time.deltaTime, 0);
        m_pTourist.transform.position += pDirection;

        Vector3 pLeftOfScreen = Camera.main.ScreenToWorldPoint(new Vector3( -40, 0 ));

        if (m_pTourist.transform.position.x < pLeftOfScreen.x)
            Object.Destroy(m_pTourist.gameObject);
    }
}