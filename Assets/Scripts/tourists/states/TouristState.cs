﻿using UnityEngine;

public class TouristState
{
    public enum StateId { IDLE, SUNBATHE, SWIM, ENTER, LEAVE, LEAVEEARLY, DEAD, HOLDING }
    public StateId m_pStateId { get; private set; }

    protected Tourist m_pTourist;
    protected Animator m_pAnimator;

    public TouristState( StateId pId, Tourist pTourist )
    {
        m_pStateId = pId;
        m_pTourist = pTourist;
        m_pAnimator = m_pTourist.m_pAnimator;
    }

    public virtual void onStateEnter()
    {

    }

    public virtual void onStateExit()
    {

    }

    public virtual void onEvent( TouristEvent pEvent )
    {

    }

    public virtual void onUpdate()
    {

    }
}