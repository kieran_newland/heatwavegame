﻿using UnityEngine;

public class TouristHoldingState : TouristState
{
    public TouristHoldingState( Tourist pTourist ) : base( StateId.HOLDING, pTourist )
    {

    }

    public override void onStateEnter()
    {
        m_pAnimator.SetBool("bHolding", true);
        SoundManager.Create(m_pTourist.gameObject, "pickupmp3", false);
    }

    public override void onStateExit()
    {
        m_pAnimator.SetBool("bHolding", false);
    }
}