﻿public class ScoreManager
{
    public float m_nScore { get; private set; }
    private const float MINTAN = 0.8f;
    private const float MAXTAN = 1.3f;
    private const float TANREWARD = 0.5f;

    public ScoreManager()
    {
        Game.score = this;
    }

    public void updateScore( float nFrontTan, float nBackTan )
    {
        float nTotalScore = 0;

        nTotalScore += calculateScore(nFrontTan);
        nTotalScore += calculateScore(nBackTan);

        m_nScore += nTotalScore;
    }

    private float calculateScore( float nTan )
    {
        if (inTanBoundary(nTan))
            return TANREWARD;

        return -TANREWARD;
    }

    private bool inTanBoundary( float nTan )
    {
        return nTan > MINTAN && nTan < MAXTAN;
    }
}