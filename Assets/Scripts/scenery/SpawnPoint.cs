﻿using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    private BoxCollider2D m_pCollider;

    public void Awake()
    {
        m_pCollider = GetComponent<BoxCollider2D>();
    }

    public Vector3 getRandomPoint()
    {
        float nRandomX = Random.value * m_pCollider.bounds.size.x;
        float nRandomY = Random.value * m_pCollider.bounds.size.y;

        return transform.position - (m_pCollider.bounds.size / 2) + new Vector3( nRandomX, nRandomY );
    }
}