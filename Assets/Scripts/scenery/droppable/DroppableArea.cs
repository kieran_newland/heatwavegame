﻿using UnityEngine;

public class DroppableArea : MonoBehaviour
{
    public bool setUpTourist( Tourist pTourist )
    {
        if (!canBeUsed())
            return false;

		playSound (pTourist);
        positionTourist(pTourist);
        setTouristState(pTourist);

        return true;
    } 

    protected virtual bool canBeUsed()
    {
        return true;
    }

	protected virtual void playSound(Tourist pTourist)
	{
		
	}

    protected virtual void positionTourist( Tourist pTourist )
    {

    }

    protected virtual void setTouristState( Tourist pTourist )
    {
        pTourist.setState(TouristState.StateId.IDLE);
    }

    public virtual void onTouristLeave( Tourist pTourist )
    {

    }
}