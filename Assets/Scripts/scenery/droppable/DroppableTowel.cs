﻿using UnityEngine;

public class DroppableTowel : DroppableArea
{
    private Tourist m_pCurrentTourist;
    private const int TOTALTOWELS = 8;

    public void Awake()
    {
        SpriteRenderer pRenderer = gameObject.GetComponent<SpriteRenderer>();

        //Get random towel image
        Sprite pSprite = Resources.Load<Sprite>( "Towels/towel_" + Mathf.FloorToInt( Random.value * TOTALTOWELS + 1 ) );
        pRenderer.sprite = pSprite;
    }

    protected override bool canBeUsed()
    {
        return m_pCurrentTourist == null;
    }

    protected override void positionTourist(Tourist pTourist)
    {
        m_pCurrentTourist = pTourist;
        pTourist.transform.position = transform.position;
    }

	protected override void playSound(Tourist pTourist)
	{
		SoundManager.Create (pTourist.gameObject, "dropmp3", false);
	}

    protected override void setTouristState(Tourist pTourist)
    {
        pTourist.setState(TouristState.StateId.SUNBATHE);
    }

    public override void onTouristLeave(Tourist pTourist)
    {
        m_pCurrentTourist = null;
    }

    public static DroppableTowel Create()
    {
        GameObject pTowelPrefab = Instantiate(Resources.Load<GameObject>("TowelPrefab"));
        DroppableTowel pTowel = pTowelPrefab.GetComponent<DroppableTowel>();

        return pTowel;
    }
}