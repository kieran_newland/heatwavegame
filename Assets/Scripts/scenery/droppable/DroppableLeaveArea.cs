﻿public class DroppableLeaveArea : DroppableArea
{
    protected override void setTouristState(Tourist pTourist)
    {
        pTourist.setState(TouristState.StateId.LEAVE);
    }
}