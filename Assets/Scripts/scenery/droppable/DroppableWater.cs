﻿using UnityEngine;

public class DroppableWater : DroppableArea
{
    private BoxCollider2D pCollider;

    public void Awake()
    {
        pCollider = GetComponent<BoxCollider2D>();
    }

	protected override void playSound(Tourist pTourist)
	{
		SoundManager.Create (pTourist.gameObject, "splashmp3", false);
	}

    protected override void setTouristState(Tourist pTourist)
    {
        pTourist.setState(TouristState.StateId.SWIM);
    }

    public bool isInWater( Vector3 pPos )
    {
        return pCollider.OverlapPoint(pPos);
    }
}