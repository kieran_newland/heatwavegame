﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMovement : MonoBehaviour {

    public float m_nOffset;
    private const float MOVESPEED = 0.7f;
    private const float MOVEDISTANCE = 0.5f;
    private float m_nCurrentMove;

	void Start () {
		
	}
	
	void Update () {
        m_nCurrentMove += Time.deltaTime * MOVESPEED;

        transform.localPosition = new Vector3(0, MOVEDISTANCE * Mathf.Sin(m_nCurrentMove + m_nOffset));
	}
}
