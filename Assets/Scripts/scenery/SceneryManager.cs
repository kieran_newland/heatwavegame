﻿using UnityEngine;
using System.Collections.Generic;

public class SceneryManager : MonoBehaviour
{
    public DroppableWater m_pWater { get; private set; }

    private const float TOWELXPADDING = 2f;
    private const float TOWELYPADDING = 3f;

    private const float MAXTOWELSWIDTH = 7;
    private const float MAXTOWELSHEIGHT = 2;

    //Time of day and heat
    public const float STARTTIME = 7;
    public float m_nHoursSinceStart { get; private set; }
    private const float TIMEPERINGAMEHOUR = 5;
    private float m_nCurrentHeatRate;

    private bool m_bStarted;

    public void Awake()
    {
        Game.scenery = this;

        m_nHoursSinceStart = -0.01f;

        //Get premade assets
        m_pWater = transform.Find("Water").GetComponent<DroppableWater>();
    }

    public void startCounting()
    {
        m_bStarted = true;
    }

    public void createScene( int nTowels )
    {
        //Create a list of all positions
        List<int> aPositions = new List<int>();
        for (int i = 0; i < MAXTOWELSWIDTH * MAXTOWELSHEIGHT; i++)
            aPositions.Add(i);

        GameObject pTowelHolder = new GameObject("Towels");
        pTowelHolder.transform.SetParent(transform);

        DroppableTowel pTowel;
        int nPositionIndex, nPosition;
        for( int i = 0; i < nTowels; i++ )
        {
            pTowel = DroppableTowel.Create();

            //Get random position
            nPositionIndex = Mathf.FloorToInt(Random.value * aPositions.Count);
            nPosition = aPositions[nPositionIndex];
            aPositions.RemoveAt(nPositionIndex);

            //Place the towel in the random pos
            pTowel.transform.position = new Vector3((nPosition % MAXTOWELSWIDTH) * TOWELXPADDING - (((MAXTOWELSWIDTH-1)/ 2) * TOWELXPADDING), -Mathf.Floor(nPosition / MAXTOWELSWIDTH) * TOWELYPADDING);
            pTowel.transform.SetParent(pTowelHolder.transform);
        }
    }

    public void onUpdate()
    {
        if (!m_bStarted)
            return;

        m_nHoursSinceStart += Time.deltaTime / TIMEPERINGAMEHOUR;
    }

    public bool isGameFinished()
    {
        return m_nHoursSinceStart > 12;
    }
}