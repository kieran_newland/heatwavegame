﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {

    private SceneryManager m_pScenery;
    private ScoreManager m_pScore;
    private LevelData m_pLevelData;

    private bool m_bGameOver;
	private bool m_bMusicPlaying;

	void Start () {
        //Get level info
        TextAsset pLevelData = Resources.Load<TextAsset>("levels/level" + Game.level);
        Debug.Log("Loading: " + "levels/level" + Game.level + pLevelData.text);
        m_pLevelData = JsonUtility.FromJson<LevelData>(pLevelData.text);

        m_pScenery = FindObjectOfType<SceneryManager>();
        m_pScenery.createScene(m_pLevelData.towels);

        m_pScore = new ScoreManager();

        TouristManager.Create(m_pLevelData.arrivals);

        Game.fader.fade(true, startGame);
    }

    private void startGame()
    {
        if( Input.GetMouseButtonDown( 0 ) )
        {
            RaycastHit2D pHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward);

            Debug.Log(pHit.collider);
        }

		if (!m_bMusicPlaying)
		{
			m_bMusicPlaying = true;
			SoundManager.Create (gameObject, "musicmp3", true);
		}

        m_pScenery.startCounting();
    }

    public void Update()
    {
        if ( m_pScenery == null || m_pScenery.isGameFinished())
        {
            endGame();
            return;
        }

        //Only update scenery stuff if game is still running
        m_pScenery.onUpdate();
    }

    public void endGame()
    {
        if (m_bGameOver)
            return;

        m_bGameOver = true;
        Debug.Log("Game over! Final score: " + m_pScore.m_nScore);

        EndOfGameDlg.Create( m_pScore.m_nScore );

		SoundManager.Destroy (gameObject);
		m_bMusicPlaying = false;
    }
}
